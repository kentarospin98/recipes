# Rice Kheer (Chaval Ki Kheer)

Rice kheer is an Indian Dessert, often had with Puri during festivals.

1 Cup is 250ml

## Ingredients 

- 1/4 Cup Rice. 
- 1 Litre Full Fat Milk.
- 6 Table Spoons of sugar.
- 5 Whole Cardamoms. (Elaichi)
	- Alternatively: 1/2 tea spoons of Cardamom powder.
- 2 Chopped of Sliced Almonds.
- Optionally: A table spoon of Cashews, Pistachios, or Raisins. 
- Optionally: 1 Pinch of Saffron.

## Steps

1. Rinse the rice.
2. Soak the rice for 20 to 30 minutes.
3. Heat the milk on a low to medium flame. 
4. After the milk has come to a boil, strain and add the rice to the pot.
5. Slowly cook the rice over a low flame.
6. After about 10 minutes, the rice should start to get soft. Add the sugar now.
7. Cook the rice on a medium flame, stirring occasionally to ensure you don't burn the milk.
8. Add the Cardamom, Dry fruits, and Saffron.
9. Keep stirring the kheer on a low flame, until it turns into a thick pasty mixture. The rice should be completely cooked now.
10. Serve Hot, or Chilled based on preference. 

## Notes

- You might want to use rice with smaller grains, such an Ambemore. 
- If the kheer gets too thick after it cools, you can add a little milk and stir to reach the preferred consistency.
- You can add up to half a cup of rice depending on the kind of texture you like for your kheer.
