# Moong Ka Halva

Moong ka Halva is an Indian Dessert, often made during religious ceremonies.

## Ingredients

- 1 Cup Split Husked Moong Lentils.
- 1 Cup Sugar.
- 4 Cardamoms. (Elaichi)
- 10 Table Spoons of Clarified Butter. (Ghee)
- 12 Unsalted Pistachios (Pista) Sliced or Chopped.
	- Alternatively: 12 Blanched Almonds (Badam) Sliced or Chopped.
- Optionally: 5 Raisins. (Kishmish)
- 1 Cup Milk.
- 2 Cups Water.

## Steps

1. Soak Moong Dal for 5 to 6 hours.
2. Drain and Grind with around 1/4 Cup of water until its a paste.
3. Melt the Ghee in a non-stick pan.
4. Add the Moong paste slowly.
5. Stir on a medium flame. Keep stirring until the raw smell of the lentils disappears.
6. The paste will start to get Lumpy. Break the lumps with stirring. After some time the paste should start breaking down.
7. Keep the friend lentils on a medium to low flame, stirring occasionally, and start making the syrup.
8. In a large pot, mix the water, sugar, and milk. Heat it on a medium flame to dissolve the sugar.
9. Heat the syrup till it come to a boil. 
10. After the Moong Dal mixture reaches a granular consistency, and a fried aroma appears, slowly add the sugar-milk syrup. 
11. Heat and stir the halva. It should slowly start getting thicker.
12. Add the Cardamom, Pistachios, and Raisins. 
13. Simmer and stir until the Halva is thick. 
14. Serve Hot.

## Notes

- The Halva can take over an hour to cook. Don't worry about burning the lentils too much. As long as you use non-stick cookware, and keep stirring frequently, you'll be fine.
- The Halva is supposed to be a thick paste, but some people like it dry or runny. Experiment to see what you like.
- 1 Cup sugar works pretty well, but you might want to increase or decrease according to taste.
- You can used whole non-husked Cardamoms if you like. Some people don't like getting it in their mouth.
