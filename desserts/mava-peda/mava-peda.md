# Mava Peda

Mava Peda, or Khoya Peda, is an Indian Dessert, often made during religious ceremonies.
It is also known as Milk Cake in the western world.

1 Cup: 250 ml

## Ingredients

- 6 Cups Whole Milk.
- 1-2 Cups Sugar.
- 1/8 Cup Lemon Juice.
- 4 Husked Cardamoms. (Elaichi)
- 4 Table Spoons of Clarified Butter. (Ghee)
- Whole Almonds (Badam).
- Optionally: 5 Raisins. (Kishmish)

## Steps

1. Add the milk and sugar to a large pan on a medium flame until it comes to a boil.
2. Add lemon juice, and raise the heat to high, continuously stirring the milk, making sure it doesn't burn at the bottom.
3. Add the Ghee and Cardamoms. You can also use cardamom powder for a more homogeneous flavor.
4. The milk should start to curdle. Lower the flame back to medium, still stirring the milk frequently.
5. If the milk is not curdling sufficiently, set the heat back to high for a few minutes, but don't let the milk burn.
6. Once a thick consistency is reached, simmer the flame, until the consistency is like a dough, and the color is golden brown.
7. Transfer it to a baking tray or pan such that the Mava is about 4-8 cm high.
8. Keep it in the fridge for a few hours to solidify.
9. Once it has solidified, cut into small squares, and decorate with a almond on each piece.

## Notes

- This condensation process may take over 2 hours. Be patient. Don't cook on a high flame, otherwise you may end up burning the milk.
- Saffron strands may be added for taste and color.
- Experiment with the amount of sugar. Some people like Pedas sweeter than others.
