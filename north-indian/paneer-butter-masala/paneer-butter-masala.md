# Paneer Butter Masala

Paneer Butter Masala is a north Indian style gravy, often served with Roti, Naan, or Chapatis.

1 Cup is 250ml

## Ingredients

- Oil and Butter.
- 4 whole Cardamoms. (Elaichi)
- 1 Cup Onions diced.
- 1 1/2 Cups Tomatoes diced.
- 18 Cashews.
	- Alternatively: 1/2 Cup Cream.
	- Alternatively: 1/8 Cup Butter in 3/8 Cup Milk.
- 1 Tablespoon Red Chilly Powder.
- 1 1/2 Tablespoon Garam Masala.
- 1 Tablespoon Coriander Powder. (Dhaniya Powder)
- 1 Small Bay Leaf. (Tej Patta)
- 1 Inch Cinnamon Stick. (Dalcheeni)
- 3 Cloves. (Laung)
- 1 1/2 Tablespoons Ginger-Garlic Paste.
- 250 grams Paneer.
- 1/2 Tablespoons Dried Fenugreek Leaves. (Karuri Methi)
- Optional: 1/2 tablespoon sugar.

## Steps

1. Heat a tablespoon of oil in a pan.
2. Add 2 cardamom. After about 30 seconds, add 1 cup onions.
3. Once the onions start turning pink or translucent, add 1 1/2 cups of tomatoes, with about 1/4 tablespoon salt.
4. Fry for 3 to 4 minutes. Cook covered until the tomatoes start to get mushy. This will take around 5 minutes.
5. Add 1 tablespoon Garam masala, 1 tablespoon coriander powder, 12 cashews, 1/2 tablespoon chilly powder, and 1/2 tablespoon sugar.
6. Fry for about 3 more minutes, then cool the mixture.
7. Blend the mixture with 1 cup of water into a smooth paste. Set the aside.
8. Heat 2 tablespoons butter in a pan.
9. Add 1 inch cinnamon, 3 cloves, 1 small bay leaf, and 2 cardamoms. Fry for about 2 minutes.
10. Add 1 1/2 tablespoon ginger garlic paste. Cook for 3 more minutes. Make sure you don't burn the ginger.
11. Pour the blended puree into the pan. Optionally add 1/2 a tablespoon of red chilly powder, for added color and spice. Added 1/2 a cup of water.
12. Mix thoroughly. Cooked covered for about 10 minutes on a medium flame. 
13. Lower the flame, and add 250 grams of paneer, 2 tablespoons of cream, and 1/2 a tablespoon of dried fenugreek leaves. Keep cooking till the desired consistency is reached. Add 1/2 to 1 tablespoon salt or according to taste.
14. Garnish with Coriander Leaves and cream. Serve hot.

## Notes

- If cream is not available, you can mix 1/8 cup butter with 3/8 cup of milk. Mix this mixture before using it as it separates easily.
- Instead of using 12 Cashews in step 5, you can use 6 tablespoons of cream. I find that using 6 cashews and 4 tablespoons of cream works well. Add the cream during the blending process.
- If you don't want to use cream at all, add 6 more cashes in step 5, and don't add cream in steps 13 and 14.
- If the gravy is too runny, slowly evaporate the water by keeping the gravy on a low flame, occasionally stirring to make sure you don't burn it.
- Keep the paneer in hot water for 10 minutes before adding it to the gravy to make it soft.
- You can also fry the paneer in butter before adding it in the gravy, according to preference.

